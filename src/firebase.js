import firebase from "firebase";
const firebaseConfig = {
  apiKey: "AIzaSyBbd5iuWEqN07xyHwZ_E7VWTXOTlzK6Pc4",
  authDomain: "facebook-clone-87c9e.firebaseapp.com",
  databaseURL: "https://facebook-clone-87c9e.firebaseio.com",
  projectId: "facebook-clone-87c9e",
  storageBucket: "facebook-clone-87c9e.appspot.com",
  messagingSenderId: "774258468787",
  appId: "1:774258468787:web:60086af714f58dc930cdc6",
  measurementId: "G-S3W075VZMB",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;
